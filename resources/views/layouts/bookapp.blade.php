<html>
  <head>
    <title>@yield('title')</title>
  </head>
  <body>
      <header>
          <a href="/books/create" style="float: right;">オススメの本を投稿する</a>
      </header>
      <h1>@yield('title')</h1>
      <div class="content">@yield('content')</div>
  </body>
</html>

