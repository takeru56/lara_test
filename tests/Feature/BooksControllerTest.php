<?php

namespace Tests\Feature;

use Tests\TestCase;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BooksControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */

    // BooksController#index
    public function testExample()
    {
        $response = $this->get('/books');
        $response->assertSee('推薦技術書');
    }

    //booksController#create
    public function testCreate()
    {
        $response = $this->get('/books/create');
        $response-> assertSee('本の投稿');
    }
}
